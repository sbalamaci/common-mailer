package ro.fortsoft.mailer;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Component;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;
import org.springframework.util.Assert;
import ro.fortsoft.mailer.domain.Mail;
import ro.fortsoft.mailer.domain.MailSendingResult;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.util.concurrent.Future;

/**
 * @author Serban Balamaci
 */
@Component
public class MailSender {

    @Autowired
    private JavaMailSender mailSender;

    @Autowired
    private Configuration freemarkerConfiguration;

    private static final Logger log = LoggerFactory.getLogger(MailSender.class);

    @Async
    public Future<MailSendingResult> sendEmail(Mail mail, Object emailModel) {
        try {
            Assert.state(mail.getRecipients().size() >= 1, "Empty list of recipients");

            Template template = freemarkerConfiguration.getTemplate(mail.getTemplateName());
            String text = FreeMarkerTemplateUtils.processTemplateIntoString(template, emailModel);

            MimeMessage mimeMessage = mailSender.createMimeMessage();
            MimeMessageHelper mailMessage = new MimeMessageHelper(mimeMessage, false, "utf-8");

            mailMessage.setFrom(mail.getFrom());

            mailMessage.setTo(mail.getRecipients().
                    toArray(new String[mail.getRecipients().size()]));
            mailMessage.setSubject(mail.getSubject());
            mimeMessage.setContent(text, "text/html");

            mailSender.send(mimeMessage);
            return new AsyncResult<>(new MailSendingResult());
        } catch (MailException e) {
            log.error("Error sending mail", e);
            return new AsyncResult<>(new MailSendingResult(e));
        } catch (MessagingException | IOException | TemplateException e){
            log.error("Error building mail template", e);
            return new AsyncResult<>(new MailSendingResult(e));
        }
    }

}
