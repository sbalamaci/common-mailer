package ro.fortsoft.mailer.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import java.util.Properties;

/**
 * @author Serban Balamaci
 */
@Configuration
@PropertySource("classpath:mail.properties")
public class MailConfiguration {

    @Value("${mail.server.host}")
    private String host;

    @Value("${mail.server.port}")
    private Integer port;

    @Value("${mail.protocol:smtp}")
    private String protocol;

    @Value("${mail.username}")
    private String username;

    @Value("${mail.smtp.auth}")
    private String smtpAuth;

    @Value("${mail.password}")
    private String password;

    @Bean
    public JavaMailSender javaMailSender() {
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        Properties mailProperties = new Properties();
        mailProperties.put("mail.smtp.auth", smtpAuth);
        mailProperties.put("mail.smtp.starttls.enable", "true");

        mailSender.setJavaMailProperties(mailProperties);
        mailSender.setHost(host);
        mailSender.setPort(port);
        mailSender.setProtocol(protocol);
        mailSender.setUsername(username);
        mailSender.setPassword(password);
        return mailSender;
    }


}
