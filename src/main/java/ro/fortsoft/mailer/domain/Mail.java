package ro.fortsoft.mailer.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author Serban Balamaci
 */
public abstract class Mail implements Serializable {

    private String subject;

    private String from;

    private List<String> recipients = new ArrayList<>(1);

    public abstract String getTemplateName();

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public List<String> getRecipients() {
        return recipients;
    }

    public void addRecipient(String recipient) {
        recipients.add(recipient);
    }

    public void addRecipients(Collection<String> recipients) {
        this.recipients.addAll(recipients);
    }

}
