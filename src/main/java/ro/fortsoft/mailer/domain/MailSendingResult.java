package ro.fortsoft.mailer.domain;

/**
 * @author Serban Balamaci
 */
public class MailSendingResult {

    private boolean successful;

    private final Exception exception;

    public MailSendingResult() {
        this.exception = null;
        this.successful = true;
    }

    public MailSendingResult(Exception exception) {
        this.exception = exception;
    }

    public boolean isSuccessful() {
        return successful;
    }

    public Exception getException() {
        return exception;
    }
}
